This plugin overrides dotCMS classes in order to set the header "Cache-Control" on multiple asset file types. Sets [cacheability](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control#Cacheability) to "no-cache", which forces clients to check with the origin server whether the client's cached files are still valid before either returning the cached files or downloading and returning updated files from the origin server. Also forces several classes to obey the property asset.cache.max.days and sets it to 7 days, reducing the amount of time any asset may be stored by the client.

# Installation
This plugin must be built from source and deployed to dotCMS' OSGi framework.

## Building
In order to build this plugin, use the included Gradle wrapper to run the 'jar' task.

On Windows:
```
.\gradlew.bat jar
```

On Linux or MacOS:
```
./gradlew jar
```

The resulting .jar file should be located in './build/libs'.


## Deploying
In order to deploy the compiled plugin to dotCMS' OSGi framework, you may run the Gradle "load" task, upload the plugin .jar through dotCMS' Dynamic Plugins portlet, or manually place the plugin in the dotCMS instance's Apache Felix load directory.

### Dynamic Plugins Portlet
Use of dotCMS' Dynamic Plugins portlet is recommended to those without filesystem access to the dotCMS instance.

See [dotCMS' documentation on the Dynamic Plugins portlet](https://dotcms.com/docs/latest/osgi-plugins#Portlet).

### Load Task
This project's build.gradle file defines a task called "load" which builds this plugin, undeploys previous versions, and deploys the new version to the Apache Felix load directory. In order to run this task, you must set the environment variable "FELIXDIR" to the dotCMS instance's Apache Felix base directory. The load task is recommended for those with filesystem access to the dotCMS instance.

```
> FELIXDIR="$dotcms/dotcms_3.3.2/dotserver/tomcat-8.0.18/webapps/ROOT/WEB-INF/felix"
> export FELIXDIR
> ./gradlew load
```

### Filesystem
Apache Felix monitors its "load" and "undeploy" directories to install and uninstall plugin .jar files. You can move .jar files on the filesystem to load and unload them from the OSGi framework.
```
> mv "$dotcms/dotserver/tomcat-8.0.18/webapps/ROOT/WEB-INF/felix/load/edu.msu.anr.osgi.structuralintegrity.service-*.jar" "$dotcms/dotserver/tomcat-8.0.18/webapps/ROOT/WEB-INF/felix/undeploy"
> cp ./build/libs/*.jar "$dotcms/dotserver/tomcat-8.0.18/webapps/ROOT/WEB-INF/felix/load/"
```
