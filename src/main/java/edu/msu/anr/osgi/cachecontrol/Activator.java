package edu.msu.anr.osgi.cachecontrol;

import org.osgi.framework.BundleContext;
import com.dotmarketing.osgi.GenericBundleActivator;
import com.dotmarketing.util.Config;

/**
 * OSGi Activator to enable this plugin.
 */
public class Activator extends GenericBundleActivator {

    /**
     * Starts the OSGi bundle.
     * @param context OSGi context in which this bundle will run.
     * @throws Exception If the bundle's services cannot be initialized or published.
     */
    @SuppressWarnings ("unchecked")
    public void start ( BundleContext context ) throws Exception {

        //Initializing services...
        initializeServices( context );

        //Expose bundle elements
        publishBundleServices( context );

        // Set max-age
        Config.setProperty("asset.cache.control.max.days", 7);
    }

    /**
     * Stops the OSGi bundle.
     * @param context OSGi context in which this bundle will run.
     * @throws Exception If the bundle's services cannot be unpublished.
     */
    public void stop ( BundleContext context ) throws Exception {

        //Unpublish bundle services
        unpublishBundleServices();
    }

}